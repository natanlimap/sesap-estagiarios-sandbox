class Car {
    indexa = async (req, resp, next) => {
        console.log('index');
        return 0;
    }

    coisa = async (req, res, next) => {
        console.log('coisa');
        return 1;
    }

    static async lesa() {
        console.log('lese');
        return 2;
    }
};

const c = new Car();
c.coisa();
c.indexa(0,0,0)
.then((res) => {
    console.log('index finished');
});

Car.lesa();