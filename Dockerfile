FROM node:13.12.0-alpine3.10

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

# Bundle app source
COPY . /usr/src/app

EXPOSE 3000

CMD [ "npm", "start" ]
