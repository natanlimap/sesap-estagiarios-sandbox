module.exports = (app) => {
    const paths = [];
    function print(path, layer, p) {
        function split(thing) {
            if (typeof thing === 'string') {
                return thing.split('/')
            } else if (thing.fast_slash) {
                return ''
            } else {
                var match = thing.toString()
                    .replace('\\/?', '')
                    .replace('(?=\\/|$)', '$')
                    .match(/^\/\^((?:\\[.*+?^${}()|[\]\\\/]|[^.*+?^${}()|[\]\\\/])*)\$\//)
                return match
                    ? match[1].replace(/\\(.)/g, '$1').split('/')
                    : '<complex:' + thing.toString() + '>'
            }
        }
        if (layer.route) {
            layer.route.stack.forEach(print.bind(null, path.concat(split(layer.route.path))))
        } else if (layer.name === 'router' && layer.handle.stack) {
            layer.handle.stack.forEach(print.bind(null, path.concat(split(layer.regexp))))
        } else if (layer.method) {
            const method_str = layer.method.toUpperCase();
            const path_str = path.concat(split(layer.regexp)).filter(Boolean).join('/');
            //console.log('%s /%s', method_str, path_str);
            paths.push([method_str, path_str]);
            //p.push([method_str, path_str]);
        }
    }
    app._router.stack.forEach(print.bind(null, []));
    return paths;
};