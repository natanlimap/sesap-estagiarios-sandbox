const app = require('express')();
const routes = require('./routes');
const displayRoutes = require('express-routemap');

app.use(routes);

displayRoutes(app);
