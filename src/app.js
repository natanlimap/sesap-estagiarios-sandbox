require('dotenv').config();
const express = require('express');
const path = require('path');
const cookieParse = require('cookie-parser');
const logger = require('morgan');

const routes = require('./routes');
const displayRoutes = require('express-routemap');
const getroutes = require('./libs/getroutes');

const app = express();
const port = 3000;

//views
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//midwares
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParse());
app.use(express.static(path.join(__dirname, 'public')));

//routes
app.use(routes);

if (process.env.NODE_ENV == 'development') {
	const route_paths = getroutes(app);
	console.log('PATHS FINAL', route_paths);
    app.get('/_routes', (req, res) => {
		let html = '<html><body><table border="1" ><tr><th>method</th><th>path</th></tr>';
		const routesData = route_paths.map((r) => {
			html += `<td style="padding: 4px;">${r[0]}</td><td style="padding: 4px;">/${r[1]}</td></tr>\r\n`;
		});
		html += '</html></body></html>'
		res.send(html);
    });
}

//404
app.use((req, res, next) => {
	res.send('404');
});

app.listen(port, () => {
	console.log(`listening on ${port}`);
	displayRoutes(app, 'routes.log')
});

module.exports = app;
