const router = require('express').Router();
const models = require('../../models');

router.get('/', async (req, res) => {
    const usuarios = await models.Usuario.findAll();
    res.render('usuarios/index', { usuarios: usuarios })
});

router.get('/novo', (req, res) => {
    res.render('usuarios/novo');
});


router.post('/criar', async (req, res) => {
    console.log('criar', req.body)
    const usuario = await models.Usuario.create({
        nome: req.body.nome,
        email: req.body.email
    });
    res.redirect('/usuarios');
});

router.get('/ativos', (req, res) => {
    res.send('usuarios ativos');
});

module.exports = router;
