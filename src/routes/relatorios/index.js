const router = require('express').Router();
const relatoriosUsuariosRoutes = require ('./usuarios');

const authMidware = (req, res, next) => {
    //verificar se pode usar relatõrios    
    next();
}

router.use(authMidware);

router.get('/', (req, res) => {
    res.render('relatorios/index')
});

router.get('/login', (req, res) => {
    res.send('relatorios login');
});

router.use('/usuarios', relatoriosUsuariosRoutes);

module.exports = router;