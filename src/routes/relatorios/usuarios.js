const router = require('express').Router();

router.get('/', (req, res) => {
    res.send('relatórios usuarios root');
});

router.get('/pagina', (req, res) => {
    res.send('relatórios usuarios pagina');
});

router.get('/ativos', (req, res) => {
    res.send('relatórios usuarios ativos');
});

router.get('/:id', (req, res) => {
    res.send(`relatórios usuarios delete ${req.params.id}`);
});

module.exports = router;