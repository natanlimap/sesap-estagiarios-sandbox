## Para que serve um Guia de Contribuição? ##

O guia de conribuição tem o objetivo de orientar interessados a contribuir com o projeto de forma a guiá-los no processo de desenvolvimento.

Como todo projeto, prezamos pela qualidade do código e pelos cuidados com bom nível de documentação. 

Então, antes de começar a contribuir, é muito importante que você leia este guia.  Mas não se preocupe, estamos a disposição para tirar dúvidas e te ajudar.

# Onde tirar dúvidas

Para qualquer dúvida, criamos um servidor no Discord que você pode acessar pelo seguinte [neste link](https://discord.gg/zyXDqpQ).

# O que devo fazer?

As tarefas em abertos estão listadas no seguinte link https://gitlab.com/covid-192/sesap/sesap-estagiarios-sandbox/-/issues. Escolha uma e divirta-se.

Você também pode propor alguma issue. Vamos adorar sua contribuição por novas funcionalidades.

# Fluxo de trabalho

1. Faça o clone do projeto 
2. Crie uma branch a partir da branch develop
3. Faças alterações no código
4. Commit as mudanças
5. Faça push de sua nova branch para o Gitlab do projeto
6. Abra um *Merge Request* (MR) para o branch develop através [deste link ](https://gitlab.com/covid-192/sesap/sesap-estagiarios-sandbox/-/merge_requests)

Nota: ao criar a solicitação de MR, marque a opção "Delete source branch when merge request is accepted."

# Como clonar o projeto

Para realizar a cópia do repositório execute o seguinte comando:

```console
git clone https://gitlab.com/covid-192/sesap/sesap-estagiarios-sandbox.git
```

## Como criar um *branch*? ##

Um branch pode ser criado através do seguinte comando:

```console
git checkout -b feature/nome-branch
```

## Como realizar commit 

Para comitar você deverá executar o seguinte comando:

```console
git commit -m "Minha mensagem de uma linha aqui."
```


Caso precise descrever múltiplas linhas você pode digitar  `git
commit` seguido de ENTER que um editor de terminal vai abrir para que
você escreva a mensagem de *commit*. Normalmente o editor é o vi ou
nano, mas outros podem ser habilitados. Abaixo um exemplo de mensagem
composta.

```console
Cria funcionalidade de login

  - Adiciona ./login.js
  - Adiciona configuraçao de sessão
  - Atualiza no NAMESPACE.
```

As mensagens de *commit* devem ter verbos conjugados no presente do
indicativo (ele faz, completa, adiciona, remove, edita, conserta,
produz, gera, corrige, documenta, escreve, move, transforma, modifica).

## Como fazer push do branch

Para subir seu branch para o servidor do Gitlab execute o seguinte comando:

```console
git push -u origin feature/nome-branch
```

## Como criar um merge request? ##

Criar um *merge request* (requisição de junção), acesse o menu
[*Merge resquest*](https://gitlab.com/covid-192/sesap/sesap-estagiarios-sandbox/-/merge_requests)
e preencha o formulário.

Submeta para nossa análise e fique atento aos comentários dos demais voluntários.

## Quais as exigências para aceitar um MR? ##

Para que um merge request seja aceito, 3 condições precisam ser
satisfeitas:

  1. O trabalho deve estar concluído.
  2. O *branch* tem que ter *build sucess*. A vantagem, dentre muitas,
     da integração contínua, é sabermos se um ramo tem problemas de
     código. Se um *branch* não passa nas verificações do *build*,
     quando deveria passar, então algo está errado e precisa ser
     consertado.
  3. O código deve estar limpo e auto explicativo. 

## Qualidade do código

Utilizamor o sistema Sonar Qube para analisar a qualidade do código produzido. Utilize o seguinte link para acessar a página do projeto:

[Sonar Qube - IMD/Sesap](https://sonar.lii.imd.ufrn.br/dashboard?id=sesap-estagiarios-sandbox)

Você também pode nos ajudar a manter um bom nível de qualidade resolvendo algum débito técnico =)
